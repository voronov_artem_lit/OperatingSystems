#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void fiilArray(int size, int a[])
{
    for (int i = 0; i < size; ++i)
        a[i] = i;
}
void printArray(int size, int a[])
{
    for (int i = 0; i < size; ++i)
        printf("%d ", a[i]);
    printf("\n");
}
void getInt(int *size)
{
    printf("enter a integer: ");
    char str[256];
    fgets(str, 256, stdin);
    *size = atoi(str);
}

int main(void)
{
    int N;
    getInt(&N);
    int *a = (int *)malloc(N * sizeof(int));
    fiilArray(N, a);
    printArray(N, a);

    free(a);
    return 0;
}
