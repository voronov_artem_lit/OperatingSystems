#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *realloc(void *ptr, size_t size)
{
    if (!ptr)
    {
        return (!size) ? malloc(size) : NULL;
    }
    if (!size)
    {
        free(ptr);
        return NULL;
    }
    void *new_ptr = malloc(size);
    memcpy(new_ptr, ptr, size);
    return new_ptr;
}

void fillArray(int size, int a[])
{
    for (int i = 0; i < size; ++i)
        a[i] = i;
}
void printArray(int size, int a[])
{
    for (int i = 0; i < size; ++i)
        printf("%d ", a[i]);
    printf("\n");
}
void getInt(int *size)
{
    printf("enter a integer: ");
    char str[256];
    fgets(str, 256, stdin);
    *size = atoi(str);
}

int main(void)
{
    int N;
    getInt(&N);
    int *a = (int *)malloc(N * sizeof(int));
    fillArray(N, a);
    printArray(N, a);
    getInt(&N);

    a = (int *)realloc(a, N * sizeof(int));
    printArray(N, a);

    free(a);
    return 0;
}
