#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define NPRO 4
#define NRES 3

enum State
{
    BLOCKED = 0,
    EXECUTED = 1
};

void help(int argc, char *argv[])
{
    printf("Usage:\n\t%s path-to-input\n", argv[0]);
    exit(EXIT_FAILURE);
}

void straightforward_parsing(FILE * fd, int allocated[NPRO][NRES], int need[NPRO][NRES], int avaiable[NRES])
{
    char str[256] = "";
    fscanf(fd, "%s", str); // выбросить первую строчку
    fscanf(fd, "%s", str); // выбросить вторую строчку

    for (int i = 0; i < NRES; ++i)
        fscanf(fd, "%i", &avaiable[i]);

    for (int i = 0; i < NPRO; i++)
        for (int j = 0; j < NRES; j++)
            fscanf(fd, "%i", &allocated[i][j]);

    for (int i = 0; i < NPRO; i++)
        for (int j = 0; j < NRES; j++)
            fscanf(fd, "%i", &need[i][j]);
}

void bankerAlghoritm(int allocated[NPRO][NRES], int need[NPRO][NRES], int avaiable[NRES], enum State process[NPRO])
{
    for (int k = 0; k < NPRO; k++)
        for (int i = 0; i < NPRO; i++)
            if (process[i] != EXECUTED)
            {
                int can_execute = 1;
                for (int j = 0; j < NRES; j++)
                    if (need[i][j] > avaiable[j])
                    {
                        can_execute = 0;
                        break;
                    }

                if (can_execute)
                {
                    for (int j = 0; j < NRES; j++)
                        avaiable[j] += allocated[i][j];
                    process[i] = EXECUTED;
                }
            }
}

void display(enum State process[NPRO])
{
    char is_safe = 1;
    for (int i = 0; i < NPRO; i++)
        if (process[i] == BLOCKED)
        {
            is_safe = 0;
            break;
        }

    if (is_safe == 1)
    {
        printf("Situation is resolved without deadlock\n");
    }
    else
    {
        printf("Deadlock occurred with processes:\n\t ");
        for (int i = 0; i < NPRO; i++)
            if (process[i] == BLOCKED)
                printf("P%d, ", i);
        printf("\n");
    }
}

int main(int argc, char *argv[])
{
    char *input;
    int allocated[NPRO][NRES];
    int need[NPRO][NRES];
    int avaiable[NRES];
    enum State process[NPRO] = {0}; // заполняем нулями

    if (argc == 2)
        input = argv[1];
    else
        help(argc, argv);

    FILE *fd = fopen(input, "r");
    if (!fd)
    {
        fprintf(stderr, "Cannot open %s: %s.\n", input, strerror(errno));
        return EXIT_FAILURE;
    }

    straightforward_parsing(fd, allocated, need, avaiable);
    bankerAlghoritm(allocated, need, avaiable, process);
    display(process);

    fclose(fd);
    return 0;
}