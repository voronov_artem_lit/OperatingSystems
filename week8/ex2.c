#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define SIZE 1024 * 1024 * 1024

void case1(void)
{
	//simulation of memory leak with malloc and memset
	for (int i = 0; i < 10; i++)
	{
		char *mem = (char *) malloc (SIZE); // 1 Gi
		memset(mem, 0, SIZE);
		sleep(1);
	}
}
void case2(void)
{
	//simulation of memory leak with realloc
	// (Do not work)
	char *mem;
	for (int i = 0; i < 10; i++)
	{
		mem = (char *) realloc (mem, SIZE); // 1 Gi
		sleep(1);
	}
}

void case3(void)
{
	//simulation of memory leak with malloc
	// (Do not work)
	for (int i = 0; i < 10; i++)
	{
		malloc (SIZE); // 1 Gi
		sleep(1);
	}
}


int main(void)
{
	// these case lead to result: the memory goes into the swap, it ends and the OS kills the process forcibly.
	case1();
	return 0;
}