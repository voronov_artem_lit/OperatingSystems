#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/resource.h>
#include <string.h>

#define SIZE 1024 * 1024 * 1024

void memLeakStep(void)
{
    char *mem = (char *)malloc(SIZE); // 1 Gi
    memset(mem, 0, SIZE);
    sleep(1);
}

void printRusage(const struct rusage *ru)
{
    printf("ru_utime:       %ld %ld\n", ru->ru_utime.tv_sec, ru->ru_utime.tv_usec);
    printf("ru_stime:       %ld %ld\n", ru->ru_stime.tv_sec, ru->ru_stime.tv_usec);
    printf("ru_maxrss:      %ld \n", ru->ru_maxrss);
    // printf("ru_ixrss:       %ld \n", ru->ru_ixrss);
    // printf("ru_idrss:       %ld \n", ru->ru_idrss);
    // printf("ru_isrss:       %ld \n", ru->ru_isrss);
    printf("ru_minflt:      %ld \n", ru->ru_minflt);
    printf("ru_majflt:      %ld \n", ru->ru_majflt);
    // printf("ru_nswap:       %ld \n", ru->ru_nswap);
    printf("ru_inblock:     %ld \n", ru->ru_inblock);
    printf("ru_oublock:     %ld \n", ru->ru_oublock);
    // printf("ru_msgsnd:      %ld \n", ru->ru_msgsnd);
    // printf("ru_msgrcv:      %ld \n", ru->ru_msgrcv);
    // printf("ru_nsignals:    %ld \n", ru->ru_nsignals);
    printf("ru_nvcsw:       %ld \n", ru->ru_nvcsw);
    printf("ru_nivcsw:      %ld \n", ru->ru_nivcsw);
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}

int main(void)
{
    struct rusage data;
    for (int i = 0; i < 10; ++i)
    {
        memLeakStep();
        getrusage(RUSAGE_SELF, &data);
        printRusage(&data);
    }
    return 0;
}
// Шпаргалка
// struct rusage
// {
//     struct timeval ru_utime; /* user CPU time used */
//     struct timeval ru_stime; /* system CPU time used */
//     long ru_maxrss;          /* maximum resident set size */
//     long ru_ixrss;           /* integral shared memory size */
//     long ru_idrss;           /* integral unshared data size */
//     long ru_isrss;           /* integral unshared stack size */
//     long ru_minflt;          /* page reclaims (soft page faults) */
//     long ru_majflt;          /* page faults (hard page faults) */
//     long ru_nswap;           /* swaps */
//     long ru_inblock;         /* block input operations */
//     long ru_oublock;         /* block output operations */
//     long ru_msgsnd;          /* IPC messages sent */
//     long ru_msgrcv;          /* IPC messages received */
//     long ru_nsignals;        /* signals received */
//     long ru_nvcsw;           /* voluntary context switches */
//     long ru_nivcsw;          /* involuntary context switches */
// };
