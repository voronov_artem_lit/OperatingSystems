#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>

// !Эти команды могут не работать на всех системах!
#define CURSOR_UP() printf("\r\033[A")     /*перейти на строчку выше*/
#define CLEAR_LINE() printf("%c[2K\r", 27); printf("\n"); CURSOR_UP() /*удаление строчки*/

#define SHCT_MAX 3
#define SHCT_LEN 7

static const int shortcuts[SHCT_MAX][SHCT_LEN] = {{KEY_LEFTSHIFT, KEY_P, KEY_E, 0},
                                                  {KEY_LEFTSHIFT, KEY_C, KEY_A, KEY_P, 0},
                                                  {KEY_LEFTSHIFT, KEY_S, KEY_L, KEY_I, KEY_P, 0}};

static const char *const lables[SHCT_MAX] = {"I passed the Exam!",
                                             "Get some cappuccino! ",
                                             "I Wanna SleeeeP:("};


void getKey(int fd, struct input_event *ev)
{
    CLEAR_LINE();
    do
    {
        ssize_t n = read(fd, ev, sizeof(*ev));
        if (n == (ssize_t)-1)
            if (errno == EINTR)
                continue;

        if (ev->type != EV_KEY) continue;
        if (0 <= ev->value && ev->value <= 2) break;

    } while ( 1 );
}   

void eventOnEnter(const struct input_event *const ev)
{
    if (1 <= ev->value && ev->value <= 1)
        CURSOR_UP();
}

void updateStatus(const struct input_event *const ev, char key_map[KEY_MAX + 1])
{
    key_map[ev->code] = ev->value; //update status

    // if (1 <= ev->value && ev->value <= 1)
    //     printf("code=(%d): status %d\n", ev->code, key_map[ev->code/8]); 

    switch (ev->code){
        case KEY_ENTER: eventOnEnter(ev); break;
        default: break;
    }
}

void checkShortcut( const char key_map[KEY_MAX + 1])
{
    for (char i = 0; i < SHCT_MAX; ++i)
    {
        char flag = 1;
        for (char j = 0; j < SHCT_LEN; ++j)
        {
            int key = shortcuts[i][j];
            if (!key)  // когда встречается 0 в массиве - значит конец последовательности
            {
                if (flag) // проверка, что все клавиши нажаты
                    printf("%s\n", lables[i]);
                CLEAR_LINE();
                break;
            }
            flag &= key_map[key] > 0;
        }
    }
}

int main(void)
{
    const char *dev = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
    struct input_event ev;
    int fd;

    fd = open(dev, O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
        return EXIT_FAILURE;
    }

    char key_map[KEY_MAX + 1] = {0};                  //  Create a byte array the size of the number of keys and initate the array to zero's 

    while (1)
    {
        getKey(fd, &ev);
        updateStatus(&ev, key_map);
        checkShortcut(key_map);
    }
    fflush(stdout);
    close(fd);
    return 0;
}