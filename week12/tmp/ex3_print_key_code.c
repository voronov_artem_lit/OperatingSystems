#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>

//! Эти команды могут не работать на всех системах
#define CLEAR_LINE() printf("%c[2K\r", 27) /*удаление строчки*/
#define CURSOR_UP() printf("\r\033[A")       /*перейти на строчку выше*/

//// /*
//  *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  * To be honest, I found most of the code on the Internet,
//  * but in my defense I will say that I fully understand its work.
//  *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  */ Уже не актуально)
////

static const char *const evval[3] = {"RELEASED", "PRESSED ", "REPEATED"};

void eventOnEnter(const struct input_event *const ev)
{
    if (1 <= ev->value && ev->value <= 1)
        CURSOR_UP();
}

void eventOnLSift(const struct input_event *const ev)
{
    if (1 <= ev->value && ev->value <= 1)
        printf("LShift\n");
}

void printStatus(const struct input_event *const ev)
{
    if (ev->type != EV_KEY)
        return;

    // switch (ev->code){
    //     case KEY_LEFTSHIFT:eventOnLSift(ev); return;
    //     default: break;
    // }

    if (0 <= ev->value && ev->value <= 2)
    {
        CLEAR_LINE(); 
        printf("%s 0x%04x (%d)\n",
               evval[ev->value],
               (int)ev->code,
               (int)ev->code);
        CURSOR_UP();
    }

    switch (ev->code){
        case KEY_ENTER: eventOnEnter(ev); break;
        default: break;
    }
}

void getKey(int fd, struct input_event *ev)
{
    CLEAR_LINE();
    ssize_t n = read(fd, ev, sizeof(*ev));
    if (n == (ssize_t)-1)
        if (errno == EINTR)
            return getKey(fd, ev);
}

int main(void)
{
    const char *dev = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
    struct input_event ev;
    int fd;

    fd = open(dev, O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
        return EXIT_FAILURE;
    }

    while (1)
    {
        getKey(fd, &ev);
        printStatus(&ev);
    }
    fflush(stdout);
    close(fd);
    return 0;
}