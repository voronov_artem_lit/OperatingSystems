#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>

static const char *const evval[3] = {"RELEASED", "PRESSED ", "REPEATED"};

void printStatus(const struct input_event *const ev)
{
    if (ev->type != EV_KEY)
        return;

    if (0 == ev->value) printf("\n");
    if (0 <= ev->value && ev->value <= 1)
    {
        printf("%s 0x%04x (%d)\n",
               evval[ev->value],
               (int)ev->code,
               (int)ev->code);
    }

}

void getKey(int fd, struct input_event *ev)
{
    read(fd, ev, sizeof(*ev));
}

int main(void)
{
    const char *dev = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
    struct input_event ev;
    int fd;

    fd = open(dev, O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
        return EXIT_FAILURE;
    }
    while (1)
    {
        getKey(fd, &ev);
        printStatus(&ev);
    }
    fflush(stdout);
    close(fd);
    return 0;
}