#include <stdio.h>
#include <stdlib.h>

#define MAXJ 16
#define MAXL 500
//~~~~~~~~~~~~~~~~~~~~~~~Process_open~~~~~~~~~~~~~~~~~~~~~~~~~~
enum ProcessState
{
    IDLE,
    READY,
    RUNNING,
    FINISHED
};
struct Process
{
    int burst_time;   // all execution on CPU time (given)
    int arrival_time; // time when process enters into ready (given)

    // int response_time;   // first_ex_time - arrival_time
    // int turnaround_time; // exit_time − arrival_time
    // int waiting_time;    // turnaround_time − burst_time

    int exit_time;      // (completion time) time when process completes execution
    int first_ex_time;  // time when process gets CPU for first time
    int remaining_time; // remaining execution time before FINISH state
    enum ProcessState state;
};

int process_wakeup(struct Process *process, int clk_counter)
{
    if (process->state != IDLE)
        return -1;
    if (process->arrival_time != clk_counter)
        return -2;
    // printf("wakeup: at[%d] bt[%d] on time[%d]\n", process->arrival_time, process->burst_time, clk_counter);
    process->state = READY;
    return 0;
}

int process_try_finish(struct Process *process, int clk_counter)
{
    if (process->state != RUNNING)
        return -1;

    if (process->remaining_time <= 0)
    {
        process->exit_time = clk_counter;
        process->state = FINISHED;
    }
    return 0;
}

int process_stop(struct Process *process)
{
    if (process->state != RUNNING)
        return -1;

    process->state = READY;
    return 0;
}

int process_exec(struct Process *process, int clk_counter)
{
    if ((process->state != READY) && (process->state != RUNNING))
        return -1;

    // printf("exec: at[%d] bt[%d] on time[%d]\n", process->arrival_time, process->burst_time, clk_counter);

    if (process->state == READY)
    {
        process->first_ex_time = clk_counter;
        process->state = RUNNING;
    }

    process->remaining_time -= 1;

    return 0;
}

//~~~~~~~~~~~~~~~~~~~~~Process_close~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~CPU_open~~~~~~~~~~~~~~~~~~~~~~~~~~~
void CPU_clk(int job_count, struct Process jobs[], int clk_counter)
{
    // printf("time:%d \n", clk_counter);
    // 0. checking for fineshed processes
    for (int i = 0; i < job_count; ++i)
    {
        if (jobs[i].state == RUNNING)
            process_try_finish(&jobs[i], clk_counter);
    }
    // 1. checking for new processes
    for (int i = 0; i < job_count; ++i)
    {
        if (jobs[i].state != IDLE)
            continue;
        process_wakeup(&jobs[i], clk_counter);
    }
    // 2. choose the most appropriate process (FCFS)
    struct Process *more_preferred_process = NULL;

    for (int i = 0; i < job_count; ++i)
    {
        if (jobs[i].state == RUNNING)
        {
            more_preferred_process = &jobs[i];
            break;
        }

        if (jobs[i].state != READY)
            continue;

        if (!more_preferred_process)
        {
            more_preferred_process = &jobs[i];
        }
        if (more_preferred_process->burst_time > jobs[i].burst_time)
            more_preferred_process = &jobs[i];
    }
    // 3. stop other processes
    for (int i = 0; i < job_count; ++i)
    {
        if (jobs[i].state == RUNNING)
            process_stop(&jobs[i]);
    }
    // 4. execute process
    if (more_preferred_process)
        process_exec(more_preferred_process, clk_counter);
}

int CPU_monitor(int job_count, struct Process jobs[], enum ProcessState logs[])
{
    static int index = 0;
    // printf("CPU_monitor: %d", index);
    if (index >= MAXL - 2)
        return index;
    for (int i = 0; i < job_count; ++i)
    {
        logs[index] = jobs[i].state;
        index++;
    }
    return index;
}

void CPU_start(int job_count, struct Process jobs[], enum ProcessState logs[])
{
    int flag = 1;
    int clk_counter = 0;
    CPU_monitor(job_count, jobs, logs);

    while (flag)
    {
        CPU_clk(job_count, jobs, clk_counter);
        CPU_monitor(job_count, jobs, logs);

        // chek for end
        flag = 0;
        for (int i = 0; i < job_count; ++i)
            if (jobs[i].state != FINISHED)
            {
                flag = 1;
                break;
            }

        clk_counter++;
        // sleep(1);
    }
}
//~~~~~~~~~~~~~~~~~~~~~CPU_close~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int parse_file(FILE *file, struct Process jobs[])
{
    int job_count = 0;
    for (int i = 0; i < MAXJ; i++, job_count++)
    {
        if (fscanf(file, "%d,%d", &jobs[i].arrival_time, &jobs[i].burst_time) == EOF)
            break;
        jobs[i].remaining_time = jobs[i].burst_time;
        jobs[i].state = IDLE;
    }
    return job_count;
}

void print_table(int job_count, struct Process jobs[])
{
    printf("~~~~~~~~~~~Table of estimation~~~~~~~~~~~\n");
    printf("P#\tAT\tBT\tCT\tTAT\tWT\n\n");
    for (int i = 0; i < job_count; i++)
        printf("P%d\t%d\t%d\t%d\t%d\t%d\n", i,
               jobs[i].arrival_time,
               jobs[i].burst_time,
               jobs[i].exit_time,
               jobs[i].exit_time - jobs[i].arrival_time,                       // turnaround_time = exit_time     − arrival_time;
               jobs[i].exit_time - jobs[i].arrival_time - jobs[i].burst_time); // waiting_time    = exit_time     − arrival_time − burst_time;

    double av_turnaround_time = 0, av_waiting_time = 0;
    for (int i = 0; i < job_count; i++)
    {
        av_turnaround_time += jobs[i].exit_time - jobs[i].arrival_time;                   // turnaround_time
        av_waiting_time += jobs[i].exit_time - jobs[i].arrival_time - jobs[i].burst_time; // waiting_time
    }
    av_turnaround_time /= job_count;
    av_waiting_time /= job_count;
    printf("Average Turnaround Time = %f\n", av_turnaround_time);
    printf("AverageWT = %f\n", av_waiting_time);
}

void print_diagram(int job_count, enum ProcessState logs[])
{
    int size = CPU_monitor(0, 0, 0);
    int time = size / job_count;
    int max_time = 40;
    if (time > max_time)
        time = max_time;
    printf("\n~~~~~~~~~~~Diagram of life time~~~~~~~~~~~\n");
    printf("\t ▆ - RUNNING   ▢ - WAITING\n");
    printf("clk: ");
    for (int i = 0; i < time; ++i)
        printf("%2d ", i);
    printf("\n");

    for (int i = 0; i < job_count; ++i)
    {
        printf("P%2d: ", i);
        for (int j = i+job_count; j < size; j += job_count)
        {
            if (logs[j] == READY)
                printf(" ▢ ");
            else if (logs[j] == RUNNING)
                printf(" ▆ ");
            else
                printf("   ");
        }
        printf("\n");
    }
}

int main(void)
{
    struct Process jobs[MAXJ];
    enum ProcessState logs[MAXL];

    FILE *file = fopen("input.csv", "r");
    int count = parse_file(file, jobs);
    fclose(file);

    CPU_start(count, jobs, logs);
    print_table(count, jobs);
    print_diagram(count, logs);

    return 0;
}