#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXP 1000

int DEBUG_INFO = 0;

struct Frame
{
    int16_t id;
    u_int16_t R_clock;
};

struct FrameArray
{
    struct Frame *frames;
    u_int16_t SIZE;
};

struct Logger
{
    int hit;
    int miss;
};

int parse_file(FILE *file, int pages[])
{
    int _count = 0;
    for (int i = 0; i < MAXP; i++, _count++)
    {
        if (fscanf(file, "%d", &pages[i]) == EOF)
            break;
    }
    return _count;
}

int check_for_pageid(struct FrameArray *frame_array, u_int16_t pageid)
{
    for (int i = 0; i < frame_array->SIZE; ++i)
    {
        if (frame_array->frames[i].id == pageid)
            return 1;
    }
    return 0;
}

void replace_worst_page(struct FrameArray *frame_array, u_int16_t pageid)
{
    u_int16_t index = 0;
    for (int i = 0; i < frame_array->SIZE; ++i)
    {
        if (frame_array->frames[i].id == -1)
        {
            index = i;
            break;
        }
        if (frame_array->frames[i].R_clock < frame_array->frames[index].R_clock)
            index = i;
    }

    frame_array->frames[index].id = pageid;
    frame_array->frames[index].R_clock = 0;
}

void shift_frame(struct FrameArray *frame_array)
{
    for (int i = 0; i < frame_array->SIZE; ++i)
    {
        frame_array->frames[i].R_clock >>= 1;
    }
}

void update_frame(struct FrameArray *frame_array, u_int16_t pageid)
{
    for (int i = 0; i < frame_array->SIZE; ++i)
    {
        if (frame_array->frames[i].id == pageid)
            frame_array->frames[i].R_clock |= 1 << (sizeof(frame_array->frames->R_clock) * 8 - 1);
    }
}

void print_frame_snapshot(struct FrameArray *frame_array, u_int16_t pageid)
{
    printf("\n");

    for (int i = 0; i < frame_array->SIZE; ++i)
        printf("i = %d id = %d %d\n", i, frame_array->frames[i].id, frame_array->frames[i].R_clock);

    printf("current page number %d\n", pageid);
}

void start(struct FrameArray *frame_array, int count_page, int pages[], struct Logger *log)
{
    int hit;
    for (int i = 0; i < count_page; ++i)
    {
        int pageid = pages[i];
        if (DEBUG_INFO)
            print_frame_snapshot(frame_array, pageid);

        shift_frame(frame_array);
        hit = check_for_pageid(frame_array, pageid);
        log->hit += hit;
        log->miss += !hit;
        if (!hit)
            replace_worst_page(frame_array, pageid);
        update_frame(frame_array, pageid);
    }
}

void args_parser(int argc, char *argv[])
{
    if (argc == 2)
        if (!strcmp(argv[1], "--DEBAG") ||
            !strcmp(argv[1], "--debag ") ||
            !strcmp(argv[1], "-d") ||
            !strcmp(argv[1], "-D"))
            DEBUG_INFO = 1;
        else
            printf("Warning: unknown argument [%s]\n", argv[1]);
    else if (argc > 2)
        printf("Warning: many arguments\n");
    else
        printf("~You can use flags [-d (--debag) ] to print intermediate steps~\n");
}

int main(int argc, char *argv[])
{

    args_parser(argc, argv);

    struct FrameArray frame_array;

    printf("Print the number of pages:\n> ");
    char str_size[256];
    fgets(str_size, 256, stdin);
    frame_array.SIZE = atoi(str_size);

    frame_array.frames = (struct Frame *)calloc(frame_array.SIZE, sizeof(struct Frame));
    for (int i = 0; i < frame_array.SIZE; ++i)
        frame_array.frames[i].id = -1;

    int pagesid[MAXP];
    FILE *file = fopen("input.txt", "r");
    int count_page = parse_file(file, pagesid);
    fclose(file);

    struct Logger log = {0, 0};
    start(&frame_array, count_page, pagesid, &log);
    printf("Hit - %d \tMiss - %d \tRatio %f\n", log.hit, log.miss, 1. * log.hit / log.miss);

    return 0;
}