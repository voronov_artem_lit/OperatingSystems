#include <limits.h>
#include <float.h>
#include <stdio.h>

int i = INT_MAX;
float f = FLT_MAX;
double d = DBL_MAX;

int main(void){
    printf("%7s: size {%2lu} max valie {%i} \n", "Int", sizeof(i), i);
    printf("%7s: size {%2lu} max valie {%e} \n", "Float", sizeof(f), f);
    printf("%7s: size {%2lu} max valie {%E} \n", "Double", sizeof(d), d);
    return 0;
}
