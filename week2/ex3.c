#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_tree(int n){
    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < n-i; ++j)
            printf(" ");
        for (int j = 0; j < 2*i-1; ++j)
            printf("\x1b[33m*\x1b[0m");
        for (int j = 0; j < n-i; ++j)
            printf(" ");
        printf("\n");    
    }
}


int main(int argc, char *argv[]){
//    printf("%i\n",argc); 
//    for (int i = 0; i < argc; ++i)
//        printf("%s\n",argv[i]); 

    if (argc == 2)
        print_tree(atoi(argv[1]));
    return 0;
}
