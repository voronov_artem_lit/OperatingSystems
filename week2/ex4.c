#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WHT "\x1b[0m"
#define YEL "\x1b[33m"
#define GRN "\x1b[32m"

void swap(int * a, int * b){
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}


int main(int argc, char *argv[]){
       
    char str [256] = ""; //buffer
     
    printf("%s%s%s", YEL, "Enter a 1st integer: ",WHT);
    fgets(str, 256, stdin);
    int a = atoi(str);
    
    printf("%s%s%s", YEL, "Enter a 2nd integer: ",WHT);
    fgets(str, 256, stdin);
    int b = atoi(str);

    swap(&a,&b);

    printf("%s%s%s%s%s%s%s%i%s%s%s%s%s%s%s%i%s%s%s\n", // :( максимально читаемо!
         YEL,"Swapped numbers: ", GRN, "new 1st", YEL, "{", WHT, a, YEL,"}", GRN,"  new 2nd", YEL,"{", WHT, b, YEL, "}" , WHT
         );

    return 0;
}
