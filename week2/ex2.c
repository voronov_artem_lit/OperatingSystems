#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void){
    printf("\x1b[33m%16s\x1b[0m", "Write a string: ");
    
    char * str = (char*) malloc(256 * sizeof(char));
  
    if (fgets(str, 256, stdin)) {

        printf("\x1b[33m%16s\x1b[0m", "Reverse string: ");
        for (int i = strlen(str)-2; i >= 0 ; --i)
            printf("%c", str[i]);
        printf("\n");
    }

    free(str);
    
    return 0;
}
