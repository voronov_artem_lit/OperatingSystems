#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#define N 4

void *Thread(void *Int)
{
    int id = (int)Int;
    // sleep(1);
    printf("From Thread #%i\n", (int)id);
    pthread_exit(NULL);
}

int main(void)
{
    pthread_t thread_id[N];

    for (int i = 0; i < N; ++i)
    {
        printf("Create Thread #%i\n", i);
        pthread_create(thread_id + i, NULL, Thread, (void *)i);
    }

    for (int i = 0; i < N; ++i)
    {
        pthread_join(thread_id[i], NULL);
        printf("After Thread #%i\n", i);
    }

    pthread_exit(NULL);
}
/*
    Create Thread #0
    Create Thread #1
    From Thread #0
    Create Thread #2
    From Thread #1
    Create Thread #3
    From Thread #2
    From Thread #3
    After Thread #0
    After Thread #1
    After Thread #2
    After Thread #3
*/
