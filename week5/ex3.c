#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define mSleep(time) usleep((time)*1000)
#define BUFF_SIZE 3
#define maxDelay 10
int trans_count = 0;
//~~~~~~~~~~~~~~~~~
int buff[BUFF_SIZE];
int buff_count = 0;

void buff_push(int value)
{
    if (buff_count > BUFF_SIZE || buff_count < 0) return;
    buff[buff_count++] = value;
}

int buff_pop()
{
    int item = buff[--buff_count];
    buff[buff_count] = 0;
    return item;
}
//~~~~~~~~~~~~~~~~~
enum State
{
    ACTIVE,
    SLEEP
};
enum State producer_s = ACTIVE;
enum State consumer_s = ACTIVE;

void my_sleep(enum State *state)
{
    *state = SLEEP;
    while (*state == SLEEP)
        mSleep(100);
}
void wakeUp(enum State *state)
{
    *state = ACTIVE;
}
//~~~~~~~~~~~~~~~~~

void *producer(void *value)
{
    while (1)
    {
        if (buff_count == BUFF_SIZE) my_sleep(&producer_s);
        buff_push(rand()%maxDelay);
        if (buff_count == 1) wakeUp(&consumer_s);
        mSleep(5);
    }
}

void *consumer(void *value)
{
    while (1)
    {
        if (buff_count == 0) {
            mSleep(1);
            my_sleep(&consumer_s);
        }
        int time = buff_pop();
        if (buff_count == BUFF_SIZE-1 ) wakeUp(&producer_s);
        ++trans_count;
        mSleep(time);
        
    }
}

void* informator(void* value){
	int old_count = trans_count;
	while(1){
		mSleep(400);
		printf("Count of transactions = %d\n", trans_count);
		if( old_count == trans_count ){
			printf("There is a problem!!!");
			return 0;
		}
		old_count = trans_count;
	}
}


int main(void)
{
    pthread_t thread_p;
    pthread_t thread_c;
    pthread_t inform_t;
    printf("Creating producer and consumer\n");
    pthread_create(&thread_p, NULL, producer, NULL);
    pthread_create(&thread_c, NULL, consumer, NULL);
    pthread_create(&inform_t, NULL, informator, NULL);

    pthread_join(inform_t, NULL);
}
/*
    >./ex3.o 
    Creating producer and consumer
    Count of transactions = 14
    Count of transactions = 23
    Count of transactions = 35
    Count of transactions = 45
    Count of transactions = 59
    Count of transactions = 68
    Count of transactions = 80
    Count of transactions = 89
    Count of transactions = 99
    Count of transactions = 110
    Count of transactions = 119
    Count of transactions = 131
    Count of transactions = 137
    Count of transactions = 137
    There is a problem!!!
    >
*/