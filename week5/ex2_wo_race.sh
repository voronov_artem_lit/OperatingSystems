#!/bin/sh

# track when the user pressed CTRL+C
trap 'kill_me' 2
# kill all child processes
kill_me() {
    echo "\nInterrupted!"
    for i in $(seq 1 $PROCESS); do
        # eval echo \$process_PID$i
        eval kill \$process_PID$i
    done
    rm -f $FILE_NAME
    exit 1
}

# Main part of the processes. A function that increments a number in a file
smart_increment() {
    processId=$1
    sh_file=$2
    max=$3
    for i in $(seq 1 $max); do

        lockfile $sh_file.lock

        num=$(tail -n 1 $sh_file)
        echo "#$processId: $num"
        echo $(expr $num + 1) >>$sh_file

        rm -f $sh_file.lock

        sleep 0.09$processId
    done
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
### main script
# set constant
PROCESS=3
COUNTER=25
case $# in
1) PROCESS=$1 ;;
2) PROCESS=$1 && COUNTER=$2 ;;
esac

# prepare file
FILE_NAME="ex2_shared.txt"
touch $FILE_NAME
echo '0' >>$FILE_NAME

# call the process
for i in $(seq 1 $PROCESS); do
    smart_increment $i $FILE_NAME $COUNTER &
    eval process_PID$i=$!
done

wait
echo "All processes are finished!!"

num=$(tail -n 1 $FILE_NAME)
if [ $num != $(expr $PROCESS \* $COUNTER) ]; then
    echo "Race is occur! [expected: $(expr $PROCESS \* $COUNTER); recived: $num]"
else
    echo "Good work!"
fi

rm -f $FILE_NAME
exit 0
