#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
    pid_t n;

    switch (n = fork())
    {
    case -1:
        printf("Creation of a process has failed\n");
        exit(1);

    default:
        printf("Hello from parent [PID - %d]\n", getpid());
        break;

    case 0:
        printf("Hello from child [PID - %d]\n", getpid());
        break;
    }
    return 0;
}