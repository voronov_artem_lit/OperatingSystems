#include <stdlib.h>
#include <stdio.h>

void read_command(char * const command)
{
    printf("> ");
    fgets(command, 512, stdin);
};

int main()
{
    char cmd[512];
    while (1)
    {
        read_command(cmd);
        system(cmd);
    }
    return 0;
}