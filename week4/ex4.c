#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define N 16
int set_bg_flag(int flag) // (-1) -> save
{
    static int bg_flag;
    if (-1 == flag)
        return bg_flag;
    else
        return bg_flag = flag;
};

int get_bg_flag()
{
    return set_bg_flag(-1);
};

void read_command(char *cmd[N])
{
    static char input[512];
    printf("> ");
    fgets(input, 512, stdin);
    cmd[0] = strtok(input, " \n");
    int i = 0;
    while ((i < N - 1) && (cmd[i] != NULL))
        cmd[++i] = strtok(NULL, " \n");
    cmd[i] = NULL;
    set_bg_flag(!strcmp(cmd[i - 1], "&"));
};

void exe_cmd(const char *path, char *const argv[], int bg_exe)
{
    int status;
    if (!fork())
    {
        if (bg_exe)
            printf("[%d]\n ", getpid());
        execvp(path, argv);
        exit(0);
    }
    sleep(1);
    if (bg_exe)
        waitpid(-1, &status, 0);
};

int main()
{
    char *cmd[N];
    while (1)
    {
        read_command(cmd);
        exe_cmd(cmd[0], cmd, get_bg_flag());
    }
    return 0;
}