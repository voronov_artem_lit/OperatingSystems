WHT="\033[0m"
YEL="\E[1;33m"

echo -e $YEL"\n #Create file 'lofs.img' with size = 50M "$WHT
dd if=/dev/zero of=lofs.img bs=1M count=50

echo -e $YEL"\n #It's here: "$WHT
ls -lh --color=auto

echo -e $YEL"\n #Look to loops before create a new: "$WHT
losetup -a

echo -e $YEL"\n #Create it and look carefully at the loop-number: "$WHT
sudo losetup -v -fP lofs.img
losetup -a | grep "lofs.img" | grep "loop" --color=auto

loopnumber=$(losetup -a | grep "lofs.img" | grep "loop")
loopnumber=${loopnumber%%:*}

echo -e $YEL"\n #Format the image so that it can have a file system: "$WHT
mkfs ex4 -v -F ./lofs.img

echo -e $YEL"\n #Create a new directory 'lofsdisk': "$WHT
mkdir lofsdisk -p
ls --color=auto

echo -e $YEL"\n #Mount the new directory 'lofsdisk': "$WHT
sudo mount -o loop $loopnumber ./lofsdisk -v

echo -e $YEL"\n #See disk information: "$WHT
df -hP lofsdisk/

echo -e $YEL"\n #Go indide the new FileSystem: "$WHT
cd lofsdisk 
sudo touch file.txt
sudo chmod 777 file.txt 
echo "~~~~Hello from new FileSystem~~~~~" > file.txt
echo -e $YEL"directory 'lofsdisk' has: "$WHT $(ls -a --color=auto)
echo -e $YEL"\n #cat file.txt: "$WHT
cat file.txt
cd ..

echo -e $YEL"\n #Unmount: "$WHT
sudo umount ./lofsdisk -v

echo -e $YEL"\n #Detach our loops: "$WHT
sudo losetup -d $loopnumber

echo -e $YEL" #Delete the directory and image: "$WHT
rm -rf lofsdisk lofs.img