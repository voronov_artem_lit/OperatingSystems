WHT="\033[0m"
YEL="\E[1;33m"

say_help() {
    echo "Using: $0 {run|clear|test}"
}

LOF=lofsdisk

# $1 is the name of new file
# $2 is the file contents
crate_file() {
    filename=$1
    contents=$2
    sudo touch $filename
    sudo chmod +777 $filename
    echo $contents >$filename
}

create_File_System() {
    dd if=/dev/zero of=lofs.img bs=1M count=50
    sudo losetup -fP lofs.img
    mkfs ex4 -F lofs.img
    mkdir $LOF -p
    sudo mount -o loop lofs.img $LOF
}

fill_directory() {
    crate_file $LOF/file1.txt "~~~~Hello from new FileSystem~~~~~"
    crate_file $LOF/file2.txt "Artem Voronov, Groub BS-03"
}

# $1 is the name of LOF
# $2 is the list of comands
copy_dependencies() {
    LOF=$1
    comands=$2

    sudo mkdir $LOF/bin -p
    for d in $comands; do
        sudo cp -f "/bin/${d}" $LOF/bin
        list="$(ldd /bin/${d} | egrep -o '/lib.*\.[0-9]')"
        for i in $list; do sudo mkdir -p $(dirname "${LOF}${i}") && sudo cp -f "$i" "${LOF}${i}"; done
    done
}

add_ls_libs() {
    copy_dependencies $LOF "bash cat echo ls"

}

test1() {
    echo -e $YEL"  Original Root directory has:   "$WHT
    ls -a / --color=auto
}

test2() {
    echo -e $YEL"  Root directory 'LOF' has:   "$WHT
    sudo chroot $LOF ls -a / --color=auto
}

# $@ list of tests
testing() {
    tests=$@
    echo "" > ex2.txt
    for d in $tests; do
        ${d}
        ${d} >> ex2.txt
    done
}

main() {
    create_File_System
    fill_directory
    add_ls_libs
}

run(){
    sudo chroot $LOF
}

clear() {
    loopnumber=$(losetup -a | grep "lofs.img" | grep "loop")
    loopnumber=${loopnumber%%:*}
    sudo umount $LOF -v
    sudo losetup -d ${loopnumber}
    rm -rf $LOF lofs.img
}

if [[ $1 == "run" ]]; then
    clear
    main
    run
elif [[ $1 == "clear" ]]; then
    clear
elif [[ $1 == "test" ]]; then
    clear
    main
    testing test1 test2
elif [[ $1 != "none" ]]; then
    echo "Unknown first argument"
    say_help
    exit
fi
