#include <stdio.h>

// Helpful macro for array size
#define ARR_SIZE(x) (sizeof(x) / sizeof((x)[0]))

void swap(int *a, int *b)
{
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

void bubble_sort(int N, int arr[])
{
    for (int i = 0; i < N - 1; i++)
        for (int j = 1; j < N - i; j++)
            if (arr[j - 1] > arr[j])
                swap(arr + j - 1, arr + j);
}

void print_arr(int N, int *arr)
{
    for (int i = 0; i < N; i++)
        printf("%i\n", arr[i]);
}

int main()
{
    int arr[10] = {6, 2, 1, 232, 8, 3, 5, 9, 4, 7};
    printf("Initial array: \n");
    print_arr(ARR_SIZE(arr), arr);
    bubble_sort(ARR_SIZE(arr), arr);
    printf("Sorted array: \n");
    print_arr(ARR_SIZE(arr), arr);
    return 0;
}