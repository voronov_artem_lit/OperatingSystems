#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

struct Node
{
    int data;
    struct Node *next;
};

void print_list(const struct Node * const head)
{
    printf("%i", head->data);
    const struct Node *ptr = head->next;
    while (ptr)
    {
        printf(", %i", ptr->data);
        ptr = ptr->next;
    }
    printf(";\n ");
}

void insert_node(struct Node * const head, struct Node * const newnode, struct Node * const prevnode)
{
    // check available prevnode //
    struct Node *ptr = head;
    int flag = 0;
    while (ptr)
    {
        if (ptr == prevnode)
        {
            flag++;
            break;
        }
        ptr = ptr->next;
    }
    assert(flag && "prevnode is not in list");
    //~~~~~~~~~~~~~~~~~~~~~~~~~~//
    newnode->next = prevnode->next;
    prevnode->next = newnode;
}

void delete_node(struct Node * const head, struct Node * delnode)
{
    // check available delnode //
    struct Node *prevnode = head;
    int flag = 0;
    while (prevnode)
    {
        if (prevnode->next == delnode)
        {
            flag++;
            break;
        }
        prevnode = prevnode->next;
    }
    assert(flag && "delnode is not in list");
    //~~~~~~~~~~~~~~~~~~~~~~~~~~//
    prevnode->next = delnode->next;
    // free(delnode);
}

int main(){
  struct Node head = {0,NULL}, n1 = {1,NULL}, n2 = {2,NULL}, n3 = {3,NULL};
  printf("Initial list:\n  ");
  print_list(&head);

  insert_node(&head, &n2,  &head);
  printf("\nList after inserting 2 after head:\n  ");
  print_list(&head);

  insert_node(&head, &n3, &head);
  printf("\nList after inserting 3 after head:\n  ");
  print_list(&head);

  insert_node(&head, &n1, &n2);
  printf("\nList after inserting 1 after 2:\n  ");
  print_list(&head);

  delete_node(&head, &n2);
  printf("\nList after deleting 2:\n  ");
  print_list(&head);

  delete_node(&head, &n1);
  insert_node(&head, &n1, &head);
  printf("\nList after deleting 1 and inseting it after head:\n  ");
  print_list(&head);

  return 0;
}