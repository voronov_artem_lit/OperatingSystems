echo "Artem Voronov, Groub BS-03" > _ex3.txt

echo "Initial permitions:" > ex3.txt
ls -l _ex3.txt >> ex3.txt; echo >> ex3.txt

echo "Remove execute permission for everybody:" >> ex3.txt
chmod -x   _ex3.txt -v >> ex3.txt; echo >> ex3.txt
echo "Grant all permissions to owner and others:" >> ex3.txt
chmod +707 _ex3.txt -v >> ex3.txt; echo >> ex3.txt
echo "Make group permissions equal to user permissions:" >> ex3.txt
chmod g=u  _ex3.txt -v >> ex3.txt; echo >> ex3.txt

echo "660 gives read and write permissions to owner and group, removes all permissions to others" >> ex3.txt
echo "775 gives all permissions to owner and group, read and execute permissions to others" >> ex3.txt
echo "777 gives all permissions to everyone" >> ex3.txt
rm -rf _ex3.txt