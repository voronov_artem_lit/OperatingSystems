WHT="\033[0m"
YEL="\E[1;33m"

echo 'I love Linux' > file.txt
cat file.txt
echo -e $YEL"create hard links" $WHT
ln -v file.txt hardlink1
ln -v file.txt hardlink2
echo -e $YEL"list all files with inodes" $WHT
ls --color=auto -li file.txt hardlink?
echo -e $YEL"remove 1st hardlin" $WHT
rm hardlink1
ls --color=auto -li file.txt hardlink?
echo -e $YEL"remove source file" $WHT
rm file.txt
ls --color=auto -li file.txt hardlink?
echo -e $YEL"but we can still access original file.txt data" $WHT
cat hardlink2
echo -e $YEL"to get rid of file.txt data delete all hard links too" $WHT
rm hardlink2
echo -e $YEL"error error all data gone" $WHT
cat file.txt hardlink?
ls --color=auto -li file.txt hardlink?