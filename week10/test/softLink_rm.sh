WHT="\033[0m"
YEL="\E[1;33m"

echo " I love Linux " > file.txt
cat file.txt
echo -e $YEL" create hard links "$WHT
ln -sv file.txt softlink1 
ln -sv file.txt softlink2 
echo -e $YEL" list all files with inodes "$WHT
ls -li file.txt softlink? --color=auto
echo -e $YEL" remove 1st softlink "$WHT
rm softlink1
ls -li file.txt softlink? --color=auto
echo -e $YEL" remove source file "$WHT
rm file.txt
ls -li softlink? --color=auto      
echo -e $YEL" Now we can not access to original file.txt data "$WHT
cat softlink2
rm softlink2