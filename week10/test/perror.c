// perror.c
/* This program attempts to open a file
 * Because this file probably doesn't exist,
 * an error message is displayed.
 */

#include <stdio.h>

int main(int argc, char **argv)
{
    FILE *fh = fopen(argv[1], "r");
    perror("Open file");
    if (fh)fclose(fh);

    remove(argv[0]); //self-removal
}