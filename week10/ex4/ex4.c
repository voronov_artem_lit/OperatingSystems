#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

#define FCNT 16

void findLinks(char *dirPath, ino_t inode, char links[][FCNT])
{
	DIR *dirp = opendir(dirPath);
	if (!dirp)
	{
		perror("Open directory");
		return;
	}
	struct dirent *dp;
	char index = 1;
	while ((dp = readdir(dirp)) != NULL)
		if (dp->d_ino == inode)
			strcpy(links[index++], dp->d_name);

	strcpy(links[index],"");
	if (dirp)closedir(dirp);
}

void printLinks(char size, char files[512][FCNT][FCNT])
{
	if (!size) return;
	printf("File — Hard Links:\n");
	for (int i = 0; i < size; ++i)
	{
		printf("%s — ", files[i][0]);
		for (int j = 1; files[i][j][0]; ++j)
			printf("%s ", files[i][j]);
		printf("\n");
	}
}

int main(void)
{

	DIR *dirp = opendir("tmp");
	if (!dirp)
	{
		perror("Open directory");
		return 1;
	}

	struct dirent *dp;
	struct stat sb;
	char files[512][FCNT][FCNT]; //  findex -> [file name , links array [] ]

	int findex = 0;
	while ((dp = readdir(dirp)) != NULL)
	{
		char name[128] = "tmp/";
		strcat(name, dp->d_name);

		if (stat(name, &sb) == -1)
		{
			printf("(%s):", name);
			perror("lstat");
			continue;
		}
		if (!S_ISREG(sb.st_mode))
			continue;
		if (sb.st_nlink < 2)
			continue;
		strcpy(files[findex][0], dp->d_name);
		findLinks("tmp", dp->d_ino, files[findex++]);
	}
	printLinks(findex, files);

	if (dirp) closedir(dirp);
	return 0;
}