mkdir week01 -p
echo "Artem Voronov, Groub BS-03" > week01/file.txt

mkdir week10 -p

ln week01/file.txt week10/_ex2.txt -f

inode=$(ls -i week01/file.txt)
inode=${inode% *}

find week10 -inum $inode > ex2.txt
find week10 -inum $inode -exec rm {} \; >> ex2.txt 

# rm -f week01/file.txt week10/_ex2.txt