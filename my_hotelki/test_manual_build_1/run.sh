#!/bin/sh
# preprocessing
cpp ./src/main.c -o ./tmp/main.i

# to assembler code
gcc -S -fverbose-asm -masm=intel ./tmp/main.i -o ./tmp/main.s

# to object file
as ./tmp/main.s -o ./tmp/main.o

# linking   
gcc -o main.out ./tmp/main.o
# ld -o main.out ./tmp/main.o -lc -dynamic-linker /lib64/ld-linux-x86-64.so.2

# loading
./main.out 

# [gcc -c main.c -o main.o] # compiles source files without linking.