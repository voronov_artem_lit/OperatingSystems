#include <pthread.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "test.h"

#define DELAY 10    /*in ms*/
#define INTER_OUT 0 /*1 - Yes, 0 - No*/

pthread_mutex_t mutex;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void *straight_forward_increment(void *int_t)
{
    int count = ((struct Inc_t *)int_t)->count;
    int *num = ((struct Inc_t *)int_t)->sh_var;
    int id = ((struct Inc_t *)int_t)->id;

    if (!count)
        return 0;
    if (INTER_OUT)
        printf("\tStart thread[%d] num[%d]\n", id, *num);

    do
    {
        ++*num;
        if (INTER_OUT)
            printf(" [%d] - num: %3d\n", id, *num);
    } while (--count && !mSleep(DELAY));

    if (INTER_OUT)
        printf("\t~Finish thread[%d]\n", id);
    pthread_exit((void *)0);
}

void *system_mutex_increment(void *int_t)
{
    int count = ((struct Inc_t *)int_t)->count;
    int *num = ((struct Inc_t *)int_t)->sh_var;
    int id = ((struct Inc_t *)int_t)->id;

    if (!count)
        return 0;
    if (INTER_OUT)
        printf("\tStart thread[%d] num[%d]\n", id, *num);

    do
    {
        pthread_mutex_lock(&mutex);
        ++*num;
        pthread_mutex_unlock(&mutex);
        if (INTER_OUT)
            printf(" [%d] - num: %3d\n", id, *num);
    } while (--count && !mSleep(DELAY));

    if (INTER_OUT)
        printf("\t~Finish thread[%d]\n", id);
    pthread_exit((void *)0);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main(int argc, char *argv[])
{
    void *(*test_name[])(void *) = {
        straight_forward_increment,
        system_mutex_increment,
    };
    const int N_TEST = sizeof(test_name) / sizeof(test_name[0]);

    int thr_count = 4;
    int inc_count = 15;
    assert((argc <= 3) && "Need two parameter: 'number_of_thread' and 'amount_of_increments'");
    switch (argc) {
        case 3: inc_count = atoi(argv[2]);
        case 2: thr_count = atoi(argv[1]);
    }
    printf("Number_of_thread '%d', Amount_of_increments'%d' \n", thr_count, inc_count);

    pthread_mutex_init(&mutex, NULL);

    struct TestResult res[N_TEST];
    for (int i = 0; i < N_TEST; ++i)
    {
        //printf("--Perform test#%d\n",i);
        res[i] = exe_test(thr_count, inc_count, test_name[i]);
    }

    pthread_mutex_destroy(&mutex);

    printf("\n~~~~~~~~~~~~~Evaluation~~~~~~~~~~~~~\n");
    for (int i = 0; i < N_TEST; ++i)
    {
        printf("Test %d [%fms]: ", i, res[i].time);
        if (res[i].cnt_err)
            printf("error race is occur! [conlicts: '%d' out of '%d' increments]\n", res[i].cnt_err, thr_count * inc_count);
        else
            printf("passed! [conlicts: '0']\n");
    }
    pthread_exit(NULL);
}