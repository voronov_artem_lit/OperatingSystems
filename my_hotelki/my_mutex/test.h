#ifndef TEST_H
#define TEST_H
#include <unistd.h>

#define mSleep(time) usleep((time)*1000)

struct TestResult
{
    double time;
    int cnt_err;
};

struct Inc_t
{
    int count;   // amount_of_increments
    int *sh_var; // shared variable
    int id;      // shared variable
};

struct TestResult exe_test(int t_cnt, int i_cnt, void *(*func)(void *));

#endif // TEST_H