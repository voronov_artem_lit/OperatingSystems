#include "test.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

struct TestResult exe_test(int t_cnt, int i_cnt, void *(*func)(void *))
{
    clock_t start, end;

    int shared_variable = 0;
    pthread_t *jobs = (pthread_t *)malloc(t_cnt * sizeof(pthread_t));

    start = clock();

    struct Inc_t inc_t;
    for (int i = 0; i < t_cnt; ++i)
    {
        inc_t = (struct Inc_t){i_cnt, &shared_variable, i};
        pthread_create(&jobs[i], NULL, func, (void *)&inc_t);
    }

    for (int i = 0; i < t_cnt; ++i)
    {
        pthread_join(jobs[i], NULL);
    }

    end = clock();

    free(jobs);

    return (struct TestResult){difftime(end, start) / CLOCKS_PER_SEC, t_cnt * i_cnt - shared_variable};
}