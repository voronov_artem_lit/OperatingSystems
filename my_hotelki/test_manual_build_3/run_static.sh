#!/bin/sh
YEL='\033[1;33m'
NC='\033[0m' # No Color

mkdir tmp

gcc -Wall -c ./src/doSomething.c -o ./tmp/doSomething.o
gcc -Wall -c ./src/doubling.c -o ./tmp/doubling.o

# create my static library File must have name lib<lib_name>.a!!
ar crs ./lib/libworker.a ./tmp/doSomething.o ./tmp/doubling.o

echo "${YEL}File staticlib:${NC}"
file ./lib/libworker.a
echo "\n${YEL}Symbol table staticlib:${NC}"
nm -s -a -S ./lib/libworker.a
echo "\n${YEL}Size staticlib:${NC}"
size  ./lib/libworker.a 
# objdump -d ./lib/libworker.a  #is intresting

# linking
gcc -Wall -c ./src/main.c -o ./tmp/main.o
gcc -o main ./tmp/main.o -Llib -lworker

echo "\n${YEL}Size main:${NC}"
size  main

# loading
echo "\n${YEL}Run:${NC}"
./main              

rm -r ./tmp