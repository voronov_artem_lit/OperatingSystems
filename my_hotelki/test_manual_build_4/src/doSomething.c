#include "../include/worker.h"
#include <stdio.h>

int doSomething(){
    puts("Hello, I am a shared library");
    int a = 1;
    int b = 10;
    return doubling(b) + a;
}