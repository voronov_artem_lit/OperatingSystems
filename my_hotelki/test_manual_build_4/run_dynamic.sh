#!/bin/sh
YEL='\033[1;33m'
NC='\033[0m' # No Color

mkdir tmp

## create my dynamic library File. Optional:{-Wl,-option,value1,value2...}
gcc -shared -Wall -fPIC -o ./lib/libworker.so ./src/doSomething.c ./src/doubling.c #-Wl,-soname,libworker.so.1 #-soname derimo polnoe!!!


## print some info:
echo "${YEL}File dynamiclib:${NC}"
file ./lib/libworker.so
echo "\n${YEL}Symbol table dynamiclib:${NC}"
nm -s -S ./lib/libworker.so
echo "\n${YEL}Soname dynamiclib:${NC}"
objdump -p ./lib/libworker.so | grep SONAME
echo "\n${YEL}Size dynamiclib:${NC}"
du -h  ./lib/libworker.so

## Form binary to assembler (disasembling)
# objdump -d ./lib/libworker.so.1.2.3.4 # is intresting

## linking
##~~~~~~~ case 1 ~~~~~~~~##
gcc -Wall -o main ./src/main.c -Llib -lworker

##~~~~~~~ case 2 ~~~~~~~~##
gcc -Wall -o main2 ./src/main.c -Llib -lworker -Wl,-rpath=./lib 

## loading
##~~~~~~~ case 1 ~~~~~~~~##
echo "\n${YEL}Run 1:${NC}"
sudo ldconfig ~/workspace/OperatingSystems/my_hotelki/test_manual_build_4/lib
./main

echo "${YEL}Size main:${NC}"
du -h  main 
echo "${YEL}NEEDED main:${NC}"
objdump -p main | grep NEEDED
echo "${YEL}RUNPATH main:${NC}"
objdump -p main | grep RUNPATH
echo "${YEL}Libraries main:${NC}"
ldd main



##~~~~~~~ case 2 ~~~~~~~~##
echo "\n${YEL}Run 2:${NC}"
./main2

echo "${YEL}NEEDED main:${NC}"
objdump -p main2 | grep NEEDED
echo "${YEL}RUNPATH main2:${NC}"
objdump -p main2 | grep RUNPATH
echo "${YEL}Libraries main2:${NC}"
ldd main2

## for a more complete understanding
# objdump -p main


sudo ldconfig
rm -r ./tmp