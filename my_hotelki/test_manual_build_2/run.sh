#!/bin/sh
# preprocessing
/usr/lib/gcc/x86_64-linux-gnu/9/cc1 -E -quiet ./src/main.c -o ./tmp/main.i
/usr/lib/gcc/x86_64-linux-gnu/9/cc1 -E -quiet ./src/worker.c -o ./tmp/worker.i
#  /usr/lib/gcc/x86_64-linux-gnu/9/cc1 -E -quiet -v -imultiarch x86_64-linux-gnu ./src/main.c -o ./tmp/main.i -mtune=generic -march=x86-64 -fasynchronous-unwind-tables -fstack-protector-strong -Wformat -Wformat-security -fstack-clash-protection -fcf-protection
# gcc -E ./src/worker.c -o ./tmp/worker.i
# gcc -E ./src/main.c -o ./tmp/main.i
# use: "gcc -v ./src/main.c -o a.out" to see all stadies

# to assembler code
/usr/lib/gcc/x86_64-linux-gnu/9/cc1 -fpreprocessed -quiet ./tmp/main.i -o ./tmp/main.s
/usr/lib/gcc/x86_64-linux-gnu/9/cc1 -fpreprocessed -quiet ./tmp/worker.i -o ./tmp/worker.s 
# /usr/lib/gcc/x86_64-linux-gnu/9/cc1 -fpreprocessed ./tmp/main.i -quiet -dumpbase main.i -mtune=generic -march=x86-64 -auxbase-strip ./tmp/main.s -version -o ./tmp/main.s -fasynchronous-unwind-tables -fstack-protector-strong -Wformat -Wformat-security -fstack-clash-protection -fcf-protection
# gcc -S ./tmp/main.i -o ./tmp/main.s
# gcc -S ./tmp/worker.i -o ./tmp/worker.s  

# to object file
as -o ./tmp/main.o ./tmp/main.s  
as -o ./tmp/worker.o ./tmp/worker.s 

# linking
ld -o prog.out ./tmp/main.o ./tmp/worker.o -lc -dynamic-linker /lib64/ld-linux-x86-64.so.2 
# ld -plugin /usr/lib/gcc/x86_64-linux-gnu/9/liblto_plugin.so -plugin-opt=/usr/lib/gcc/x86_64-linux-gnu/9/lto-wrapper -plugin-opt=-fresolution=/tmp/ccch5tTI.res -plugin-opt=-pass-through=-lgcc -plugin-opt=-pass-through=-lgcc_s -plugin-opt=-pass-through=-lc -plugin-opt=-pass-through=-lgcc -plugin-opt=-pass-through=-lgcc_s --build-id --eh-frame-hdr -m elf_x86_64 --hash-style=gnu --as-needed -dynamic-linker /lib64/ld-linux-x86-64.so.2 -pie -z now -z relro /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/Scrt1.o /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/crti.o /usr/lib/gcc/x86_64-linux-gnu/9/crtbeginS.o -L/usr/lib/gcc/x86_64-linux-gnu/9 -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu -L/usr/lib/gcc/x86_64-linux-gnu/9/../../../../lib -L/lib/x86_64-linux-gnu -L/lib/../lib -L/usr/lib/x86_64-linux-gnu -L/usr/lib/../lib -L/usr/lib/gcc/x86_64-linux-gnu/9/../../.. ./tmp/main.o ./tmp/worker.o -lgcc --push-state --as-needed -lgcc_s --pop-state -lc -lgcc --push-state --as-needed -lgcc_s --pop-state /usr/lib/gcc/x86_64-linux-gnu/9/crtendS.o /usr/lib/gcc/x86_64-linux-gnu/9/../../../x86_64-linux-gnu/crtn.o -o prog2.out
# gcc -o a.out ./tmp/main.o ./tmp/worker.o


# loading
./prog.out                